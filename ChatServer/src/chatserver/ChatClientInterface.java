package chatserver;

public interface ChatClientInterface
{
    String getName();

    /**
     * @param message
     */
    void receive(String message);
}
