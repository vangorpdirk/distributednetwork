package chatserver;

import java.util.ArrayList;
import java.util.List;

public class ChatServerImpl implements ChatServerInterface
{
    private List<ChatClientInterface> clients;

    public ChatServerImpl()
    {
        this.clients = new ArrayList<>();
    }


    public void register(ChatClientInterface client)
    {
        clients.add(client);
        send("server", client.getName() + " has entered the room");
    }


    public void unregister(ChatClientInterface client)
    {
        clients.remove(client);
        send("server", client.getName() + " has left the room");
    }

    @Override
    public void send(String name, String message)
    {
        for (ChatClientInterface client : clients)
        {
            client.receive(name + ": " + message);
        }
    }
}
