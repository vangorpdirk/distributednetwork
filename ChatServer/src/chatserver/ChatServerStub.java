package chatserver;

import communication.MessageManager;
import communication.MethodCallMessage;
import communication.NetworkAddress;

import java.util.Objects;
import java.util.logging.Logger;

public class ChatServerStub implements ChatClientInterface
{
    private final Logger logger = Logger.getLogger(ChatServerStub.class.getName());
    private NetworkAddress clientaddress;
    private MessageManager messageManager;

    public ChatServerStub(NetworkAddress clientaddress)
    {
        this.clientaddress = clientaddress;
        this.messageManager = new MessageManager();
    }

    @Override
    public String getName()
    {
        MethodCallMessage callMessage = new MethodCallMessage(messageManager.getMyAddress(), "getName");
        messageManager.send(callMessage, clientaddress);
        MethodCallMessage reply = messageManager.wReceive();

        if (!"result".equals(reply.getMethodName()))
        {
            return "";
        }

        return reply.getParameter("client");
    }

    @Override
    public void receive(String message)
    {
        MethodCallMessage callMessage = new MethodCallMessage(messageManager.getMyAddress(), "receive");
        callMessage.setParameter("message", message);
        messageManager.send(callMessage, clientaddress);
        checkEmptyReply();
    }

    private void checkEmptyReply()
    {
        String value = "";
        while (!"Ok".equals(value))
        {
            MethodCallMessage reply = messageManager.wReceive();
            if (!"result".equals(reply.getMethodName()))
            {
                continue;
            }

            value = reply.getParameter("result");
        }
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChatServerStub that = (ChatServerStub) o;
        return Objects.equals(clientaddress, that.clientaddress);
    }

    @Override
    public int hashCode()
    {
        return clientaddress != null ? clientaddress.hashCode() : 0;
    }
}
