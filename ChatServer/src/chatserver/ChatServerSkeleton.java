package chatserver;

import communication.MessageManager;
import communication.MethodCallMessage;
import communication.NetworkAddress;

import java.util.logging.Logger;

public class ChatServerSkeleton
{
    private final MessageManager messageManager;
    private final ChatServerInterface chatServer;
    private final Logger logger = Logger.getLogger(ChatServerSkeleton.class.getName());

    private ChatServerSkeleton()
    {
        this.messageManager = new MessageManager();
        logger.info("Listens to: " + messageManager.getMyAddress());
        this.chatServer = new ChatServerImpl();
    }

    private void sendEmptyReply(MethodCallMessage request)
    {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }

    private void handleRegister(MethodCallMessage request)
    {
        String address = request.getParameter("address");
        int port = Integer.parseInt(request.getParameter("port"));

        NetworkAddress clientAddress = new NetworkAddress(address, port);
        sendEmptyReply(request);
        chatServer.register(new ChatServerStub(clientAddress));
    }

    private void handleUnRegister(MethodCallMessage request)
    {
        String address = request.getParameter("address");
        int port = Integer.parseInt(request.getParameter("port"));

        NetworkAddress clientAddress = new NetworkAddress(address, port);
        sendEmptyReply(request);
        chatServer.unregister(new ChatServerStub(clientAddress));
    }

    private void handleSend(MethodCallMessage request)
    {
        String clientName = request.getParameter("client");
        String message = request.getParameter("message");

        sendEmptyReply(request);
        chatServer.send(clientName, message);
    }

    private void handleRequest(MethodCallMessage request)
    {
        String methodName = request.getMethodName();
        if ("register".equals(methodName))
        {
            handleRegister(request);
        } else if ("unregister".equals(methodName))
        {
            handleUnRegister(request);
        } else if ("send".equals(methodName))
        {
            handleSend(request);
        } else
        {
            logger.info("Skeleton received an unknown request." + request);
        }
    }

    private void run()
    {
        while (true)
        {
            MethodCallMessage request = messageManager.wReceive();
            handleRequest(request);
        }
    }

    public static void main(String[] args)
    {
        ChatServerSkeleton skeleton = new ChatServerSkeleton();
        skeleton.run();
    }
}
