package communication;

public class NetworkAddress
{
    private final String ipAddress;
    private final int portNumber;

    public NetworkAddress(String ipAddress, int portNumber)
    {
        this.ipAddress = ipAddress;
        this.portNumber = portNumber;
    }

    public int getPortNumber()
    {
        return portNumber;
    }

    public String getIpAddress()
    {
        return ipAddress;
    }

    public String toString()
    {
        return ipAddress + ":" + portNumber;
    }
}
