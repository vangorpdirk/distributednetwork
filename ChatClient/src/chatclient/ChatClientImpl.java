package chatclient;

import chatserver.ChatServerInterface;

public class ChatClientImpl implements ChatClientInterface
{
    private ChatServerInterface chatServer;
    private TextReceiver textReceiver;
    private String name;

    public ChatClientImpl(ChatServerInterface server, String name)
    {
        this.chatServer = server;
        this.name = name;
    }

    @Override
    public String getName()
    {
        return name;
    }

    public void send(String message)
    {
        chatServer.send(name, message);
    }

    @Override
    public void receive(String message)
    {
        if (textReceiver == null) return;
        textReceiver.receive(message);
    }


    public void setTextReceiver(TextReceiver textReceiver)
    {
        this.textReceiver = textReceiver;
    }


    public void unregister()
    {
        chatServer.unregister(this);
    }


    public void register()
    {
        chatServer.register(this);
    }
}
