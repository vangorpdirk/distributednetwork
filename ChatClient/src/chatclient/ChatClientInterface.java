package chatclient;

public interface ChatClientInterface
{
    String getName();

    /**
     * @param message
     */
    void receive(String message);

    void setTextReceiver(TextReceiver textReceiver);

    void unregister();

    void register();
}
