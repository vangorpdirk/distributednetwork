package chatclient;

import communication.MessageManager;
import communication.MethodCallMessage;

import java.util.logging.Logger;

public class ChatClientSkeleton implements Runnable
{
    private Logger logger = Logger.getLogger(ChatClientSkeleton.class.getName());
    private final MessageManager messageManager;
    private ChatClientInterface client;

    public ChatClientSkeleton(ChatClientInterface client, MessageManager manager)
    {
        this.messageManager = manager;
        this.client = client;
    }

    private void sendEmptyReply(MethodCallMessage request)
    {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }

    private void handleRequest(MethodCallMessage request)
    {
        String method = request.getMethodName();
        if ("receive".equals(method))
        {
            handleReceive(request);
        } else if ("getName".equals(method))
        {
            handleName(request);
        } else
        {
            logger.info("Skeleton received an unknown request." + request);
        }
    }

    private void handleReceive(MethodCallMessage request)
    {
        String chatMessage = request.getParameter("message");
        client.receive(chatMessage);
        sendEmptyReply(request);
    }

    private void handleName(MethodCallMessage request)
    {
        String clientName = client.getName();
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("client", clientName);
        messageManager.send(reply, request.getOriginator());
        sendEmptyReply(request);
    }

    public void run()
    {
        while (true)
        {
            MethodCallMessage request = messageManager.wReceive();
            handleRequest(request);
        }
    }
}

