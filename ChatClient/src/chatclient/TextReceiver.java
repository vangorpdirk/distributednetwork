package chatclient;

public interface TextReceiver
{
    void receive(String text);
}
