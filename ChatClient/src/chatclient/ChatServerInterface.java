package chatserver;

import chatclient.ChatClientInterface;

public interface ChatServerInterface
{
    /**
     * @param client
     */
    void register(ChatClientInterface client);

    /**
     * @param client
     */
    void unregister(ChatClientInterface client);

    /**
     * @param name
     * @param message
     */
    void send(String name, String message);
}
