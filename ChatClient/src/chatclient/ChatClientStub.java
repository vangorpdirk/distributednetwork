package chatclient;

import chatserver.ChatServerInterface;
import communication.MessageManager;
import communication.MethodCallMessage;
import communication.NetworkAddress;

import java.util.logging.Logger;

public class ChatClientStub implements ChatServerInterface
{
    private final Logger logger = Logger.getLogger(ChatClientStub.class.getName());
    private final NetworkAddress serverAddress;
    private final NetworkAddress clientAddress;
    private final MessageManager manager;

    public ChatClientStub(NetworkAddress serverAddress, NetworkAddress clientAddress)
    {
        this.serverAddress = serverAddress;
        this.manager = new MessageManager();
        this.clientAddress = clientAddress;
    }

    @Override
    public void register(ChatClientInterface client)
    {
        MethodCallMessage methodCallMessage = new MethodCallMessage(manager.getMyAddress(), "register");
        methodCallMessage.setParameter("address", clientAddress.getIpAddress());
        methodCallMessage.setParameter("port", Integer.toString(clientAddress.getPortNumber()));
        manager.send(methodCallMessage, serverAddress);
        checkEmptyReply();
    }

    @Override
    public void unregister(ChatClientInterface client)
    {
        MethodCallMessage methodCallMessage = new MethodCallMessage(manager.getMyAddress(), "unregister");
        methodCallMessage.setParameter("address", clientAddress.getIpAddress());
        methodCallMessage.setParameter("port", Integer.toString(clientAddress.getPortNumber()));
        manager.send(methodCallMessage, serverAddress);
        checkEmptyReply();
    }

    @Override
    public void send(String name, String message)
    {
        MethodCallMessage methodCallMessage = new MethodCallMessage(manager.getMyAddress(), "send");
        methodCallMessage.setParameter("client", name);
        methodCallMessage.setParameter("message", message);

        manager.send(methodCallMessage, serverAddress);
        checkEmptyReply();
    }

    private void checkEmptyReply()
    {
        String value = "";
        while (!"Ok".equals(value))
        {
            MethodCallMessage reply = manager.wReceive();
            if (!"result".equals(reply.getMethodName()))
            {
                continue;
            }
            value = reply.getParameter("result");
        }
    }
}
