import chatclient.*;
import chatserver.ChatServerInterface;
import communication.MessageManager;
import communication.NetworkAddress;

public class Start
{
    public static void main(String[] args)
    {
        if (args.length != 2)
        {
            System.err.println("Usage: java ContactsSkeleton <zipCodesIP> <zipCodesPort>");
            System.exit(1);
        }
        int port = Integer.parseInt(args[1]);
        NetworkAddress serverAddress = new NetworkAddress(args[0], port);
        MessageManager clientAddress = new MessageManager();

        ChatServerInterface chatServerInterface = new ChatClientStub(serverAddress, clientAddress.getMyAddress());
        ChatClientInterface chatClient1 = new ChatClientImpl(chatServerInterface, "lou");
        ChatClientSkeleton clientSkeleton = new ChatClientSkeleton(chatClient1, clientAddress);
        new ChatFrame(chatClient1);

        Thread thread = new Thread(clientSkeleton);

        thread.setDaemon(true);
        thread.start();

        chatClient1.register();
    }
}
